-- MariaDB dump 10.19-11.3.2-MariaDB, for osx10.19 (arm64)
--
-- Host: localhost    Database: tourenplaner
-- ------------------------------------------------------
-- Server version	11.3.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_disponent`
--

DROP TABLE IF EXISTS `tbl_disponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_disponent` (
  `ID-Mitarbeiter-Disponent` int(11) NOT NULL AUTO_INCREMENT,
  `Disponnentennummer` varchar(45) NOT NULL,
  PRIMARY KEY (`ID-Mitarbeiter-Disponent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_disponent`
--

LOCK TABLES `tbl_disponent` WRITE;
/*!40000 ALTER TABLE `tbl_disponent` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_disponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_fahrer`
--

DROP TABLE IF EXISTS `tbl_fahrer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_fahrer` (
  `ID-Mitarbeiter-Fahrer` int(11) NOT NULL AUTO_INCREMENT,
  `Fahrernummer` varchar(45) NOT NULL,
  PRIMARY KEY (`ID-Mitarbeiter-Fahrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_fahrer`
--

LOCK TABLES `tbl_fahrer` WRITE;
/*!40000 ALTER TABLE `tbl_fahrer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_fahrer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_fahrten`
--

DROP TABLE IF EXISTS `tbl_fahrten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_fahrten` (
  `ID_Fahrt` int(11) NOT NULL AUTO_INCREMENT,
  `FS_Kennzeichen_Fahrzeug` varchar(10) NOT NULL,
  `FS_ID_Disponent` int(11) NOT NULL,
  `tbl_orte_ID_Startort` int(11) NOT NULL,
  `tbl_orte_ID_Zielort` int(11) NOT NULL,
  PRIMARY KEY (`ID_Fahrt`),
  KEY `fk_tbl_Fahrten_tbl_Fahrzeug1_idx` (`FS_Kennzeichen_Fahrzeug`),
  KEY `fk_tbl_Fahrten_tbl_Disponent1_idx` (`FS_ID_Disponent`),
  KEY `fk_tbl_fahrten_tbl_orte1_idx` (`tbl_orte_ID_Startort`),
  KEY `fk_tbl_fahrten_tbl_orte2_idx` (`tbl_orte_ID_Zielort`),
  CONSTRAINT `fk_tbl_Fahrten_tbl_Disponent1` FOREIGN KEY (`FS_ID_Disponent`) REFERENCES `tbl_disponent` (`ID-Mitarbeiter-Disponent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_Fahrten_tbl_Fahrzeug1` FOREIGN KEY (`FS_Kennzeichen_Fahrzeug`) REFERENCES `tbl_fahrzeug` (`Kennzeichen`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_fahrten_tbl_orte1` FOREIGN KEY (`tbl_orte_ID_Startort`) REFERENCES `tbl_orte` (`ID_Ort`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_fahrten_tbl_orte2` FOREIGN KEY (`tbl_orte_ID_Zielort`) REFERENCES `tbl_orte` (`ID_Ort`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_fahrten`
--

LOCK TABLES `tbl_fahrten` WRITE;
/*!40000 ALTER TABLE `tbl_fahrten` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_fahrten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_fahrzeug`
--

DROP TABLE IF EXISTS `tbl_fahrzeug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_fahrzeug` (
  `Kennzeichen` varchar(10) NOT NULL,
  `SitzPlaetze` int(11) NOT NULL,
  PRIMARY KEY (`Kennzeichen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_fahrzeug`
--

LOCK TABLES `tbl_fahrzeug` WRITE;
/*!40000 ALTER TABLE `tbl_fahrzeug` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_fahrzeug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_hierarchie`
--

DROP TABLE IF EXISTS `tbl_hierarchie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_hierarchie` (
  `FS_ist_Vorgesetzter_von` int(11) NOT NULL,
  `FS_ist_Mitarbeiter_von` int(11) NOT NULL,
  KEY `fk_tbl_hierarchie_tbl_mitarbeiter1_idx` (`FS_ist_Vorgesetzter_von`),
  KEY `fk_tbl_hierarchie_tbl_mitarbeiter2_idx` (`FS_ist_Mitarbeiter_von`),
  CONSTRAINT `fk_tbl_hierarchie_tbl_mitarbeiter1` FOREIGN KEY (`FS_ist_Vorgesetzter_von`) REFERENCES `tbl_mitarbeiter` (`idtbl_mitarbeiter`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_hierarchie_tbl_mitarbeiter2` FOREIGN KEY (`FS_ist_Mitarbeiter_von`) REFERENCES `tbl_mitarbeiter` (`idtbl_mitarbeiter`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_hierarchie`
--

LOCK TABLES `tbl_hierarchie` WRITE;
/*!40000 ALTER TABLE `tbl_hierarchie` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_hierarchie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mitarbeiter`
--

DROP TABLE IF EXISTS `tbl_mitarbeiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mitarbeiter` (
  `idtbl_mitarbeiter` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Vorname` varchar(45) NOT NULL,
  `Telefonnummer` varchar(45) NOT NULL,
  `AHV_Nummer` varchar(30) NOT NULL,
  `tbl_disponent_ID` int(11) NOT NULL,
  `tbl_fahrer_ID` int(11) NOT NULL,
  PRIMARY KEY (`idtbl_mitarbeiter`),
  UNIQUE KEY `AHV_Nummer_UNIQUE` (`AHV_Nummer`),
  UNIQUE KEY `idtbl_mitarbeiter_UNIQUE` (`idtbl_mitarbeiter`),
  KEY `fk_tbl_mitarbeiter_tbl_disponent1_idx` (`tbl_disponent_ID`),
  KEY `fk_tbl_mitarbeiter_tbl_fahrer1_idx` (`tbl_fahrer_ID`),
  CONSTRAINT `fk_tbl_mitarbeiter_tbl_disponent1` FOREIGN KEY (`tbl_disponent_ID`) REFERENCES `tbl_disponent` (`ID-Mitarbeiter-Disponent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mitarbeiter_tbl_fahrer1` FOREIGN KEY (`tbl_fahrer_ID`) REFERENCES `tbl_fahrer` (`ID-Mitarbeiter-Fahrer`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mitarbeiter`
--

LOCK TABLES `tbl_mitarbeiter` WRITE;
/*!40000 ALTER TABLE `tbl_mitarbeiter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mitarbeiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_orte`
--

DROP TABLE IF EXISTS `tbl_orte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_orte` (
  `ID_Ort` int(11) NOT NULL AUTO_INCREMENT,
  `PLZ` varchar(6) DEFAULT NULL,
  `Ortsbezeichnung` varchar(45) DEFAULT NULL,
  `tbl_fahrten_ID_Fahrt` int(11) NOT NULL,
  PRIMARY KEY (`ID_Ort`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_orte`
--

LOCK TABLES `tbl_orte` WRITE;
/*!40000 ALTER TABLE `tbl_orte` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_orte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stationen`
--

DROP TABLE IF EXISTS `tbl_stationen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stationen` (
  `FS_ID_Fahrt` int(11) NOT NULL,
  `FS_ID_Ort` int(11) NOT NULL,
  `Abfahrtszeit` datetime DEFAULT NULL,
  `Ankunftszeit` datetime DEFAULT NULL,
  `FS_ID_Fahrer` int(11) NOT NULL,
  PRIMARY KEY (`FS_ID_Fahrt`,`FS_ID_Ort`),
  KEY `fk_{CBEB6D4D-E005-4950-993C-931E35FBB2D4}_idx` (`FS_ID_Fahrt`),
  KEY `fk_{ACFE9F9D-C013-4C23-9E93-8A7A284EF6AF}_idx` (`FS_ID_Ort`),
  KEY `fk_tbl_Fahrten_has_tbl_Orte_tbl_Fahrer1_idx` (`FS_ID_Fahrer`),
  CONSTRAINT `fk_tbl_Fahrten_has_tbl_Orte_tbl_Fahrer1` FOREIGN KEY (`FS_ID_Fahrer`) REFERENCES `tbl_fahrer` (`ID-Mitarbeiter-Fahrer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{ACFE9F9D-C013-4C23-9E93-8A7A284EF6AF}` FOREIGN KEY (`FS_ID_Ort`) REFERENCES `tbl_orte` (`ID_Ort`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_{CBEB6D4D-E005-4950-993C-931E35FBB2D4}` FOREIGN KEY (`FS_ID_Fahrt`) REFERENCES `tbl_fahrten` (`ID_Fahrt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stationen`
--

LOCK TABLES `tbl_stationen` WRITE;
/*!40000 ALTER TABLE `tbl_stationen` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_stationen` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-31 10:28:50
