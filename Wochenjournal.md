# m164 Wochenjournal

## Woche 1

### Recap m162

#### Fragen

1. Welche Stufen gibt es bei der Wissenstreppe? Nennen Sie diese der Reihe nach und machen Sie ein Beispiel mit einem Wechselkurs.
![Wissenstreppe](img/Wissenstreppe.png)
  Es gibt:
   - Buchstaben, Zahlen, Sonderzeichen: Bausteine
   - Daten (Nachrichten): Wertigkeit, Syntax, Regeln
   - Informationen: Bedeutung
   - Wissen: Zusammenhan
   - Kompetenz: Handeln
  Bei einem Wechselkurs sind die z.B. die Daten der Wert vom Euro, Dollar oder Franken. Die Information ist z.B, dass CHF Schweizer Franken sind oder dass in der Tabelle der Euro einen Wert von 1.2 enthält.
  Wissen ist der Zusammenhang: 1 CHF = 1.1 EUR. Die Kompetenz ist, dass ich für 1 CHF 1.1 EUR kaufen kann und weiss ob das Sinn ergib, oder ob ich damit Verlust mache.
2. Wie werden Netzwerk-Beziehungen im logischen Modell abgebildet?
  - 1:1: Jeder Wert in Tabelle A hat genau einen Wert in Tabelle B
  - 1:m: Jeder Wert in Tabelle A hat ein oder mehrere Werte in Tabelle B
  - m:mc: Jeder Wert in der Tabelle A hat kein, ein oder mehrere Werte in Tabelle B, jeder wert in Tabelle B hat ein oder mehrere Werte in Tabelle A.
3. Was sind Anomalien in einer Datenbasis? Welche Arten gibt es?
   - Sind Probleme, die während der Datenverarbeitung/Datenspeicherung auftreten können, wenn die Daten nicht richtig normalisiert sind. Diese Daten führen zu Inkonsistenz, Redundanz und  Problemen bei der Datenintegrität.
   - Einfügeanomalie: Wenn beim Einfügen von Daten die Datenbankstruktur den Anforderungen neuer Datensätze nicht gerecht wird.
   - Löschanomalie: Wenn mehrere unterschiedliche Informationen un einer einzigen Tabelle gespeichert sind, und so bei der Löschung von Daten unbeabsichtigt weitere Daten gelöscht werden.
   - Änderungsanomalie: Wenn eine Änderung an einem Datensatz nicht konsistent auf alle relevanten Datensätze angewendet wird, was zu widersprüchlichen Daten führt.
4. Gibt es redundante "Daten"? Warum?
   Es gibt keine redundanten Daten nur redundante Informationen.
5. Datenstrukturierung:  
   1. Welche zwei Aspekte können strukturiert werden?  
      - Dateninhalt und  Datenform
   2. Welche Kategorien (Abstufungen) gibt es bei der Strukturierung?
      - strukturierte, unstrukturierte und semistrukturierte Daten. (schwach, mittel, stark strukturiert)
   3. Und wie müssen die Daten in einer DB strukturiert sein?
      - 3. NF oder strukturiert, dazu gehören auch Constraints
6. Beschreiben das Bild mit den richtigen Fachbegriffen
   ![Frage7](img/Frage7.png)
   1. Name der Tabelle
   2. Datensätze
   3. Attribute
   4. Daten/Feld
   5. Tabelle/Entität
7. Welche (einschränkenden) Einstellungen zu de Attributen kennen Sie?
   1. Primary Key/Foreign Key
   2. Not Null
   3. Unique
   4. Check
   5. Auto Increment / Serial

#### Tourenplaner

![Tourenplaner](img/Tourenplaner.png)

## Woche 2

### Generalisierung/Spezialisierung Kapitel 5

Bei der Generalisierung und Spezialisierung werden mögliche Redundanzen und Anomalien verhindert. Bei der Generalisierung werden z.B. alle Attribute aus zwei Tabellen welche gleich sind (Name, Vorname etc.) zu in einer neuen Tabelle vereint. Und dann die nicht gleichen Attribute mit der Spezialisierung in separaten Tabellen behalten. Dies kann man am Beispiel von Mitarbeitern und Kunden zeigen. Es werden die gleichen Attribute in einer neuen Entität Personen Vereint (Generalisierung) und Attribute wie Mitarbeiter/Kunden Nr in Separaten Tabellen gespeichert (Spezialisierung).

### identifying relationship vs. non-identifying relationship Kaptiel 8

Eine identifying-Beziehung liegt vor, wenn der Fremdschlüssel Teil des Identifikationsschlüssels ist, wodurch der Datensatz in der referenzierenden Tabelle durch den Datensatz, auf den er verweist, mitidentifiziert wird. Ein Beispiel hierfür ist ein Raum, der durch den Fremdschlüssel auf ein Gebäude identifiziert wird. Eine non-identifying-Beziehung liegt vor, wenn der Fremdschlüssel nicht zur Identität des Datensatzes gehört und geändert werden kann, wie bei einem Mitarbeiter, dessen Fremdschlüssel auf die Abteilung verweist und jederzeit geändert werden kann.

### DB-Engines Kapitel 1

![DBS](img/DBS.jpg)

## Woche 3

PRIMARY_KEY (ID_Ausweis, ID_Person);

Sure, here's the filled markdown table:

| Datentyp | MariaDB (MySQL) | Beispiel | Bemerkung / Einstellungen |
|----------|-----------------|----------|----------------------------|
| Ganze Zahlen | INT | 123 | - |
| Natürliche Zahlen | UNSIGNED INT | 123 | Unsigned: Nur positive Werte |
| Festkommazahlen <br> (Dezimalzahlen) | DECIMAL(M[,D]) | DECIMAL(6,2) <br> 1234.56 | M=Gesamte Anzahl Stellen <br> D=Nachkommastellen|
| Aufzählungstypen | ENUM | ENUM('rot', 'grün', 'blau') | - |
| Boolean (logische Werte) | BOOL, BOOLEAN | TRUE / FALSE | - |
| Zeichen (einzelnes Zeichen) | CHAR | 'A' | Festgelegte Länge |
| Gleitkommazahlen | FLOAT | 123.456 | - |
| Zeichenkette fester Länge | CHAR | 'Hello' | Festgelegte Länge |
| Zeichenkette variabler Länge | VARCHAR | 'Hello' | Maximale Länge |
| Datum und/oder Zeit | DATE | '2024-05-31' | - |
| Zeitstempel| TIMESTAMP | '2024-05-31 12:34:56' | Automatisch aktualisiert |
| Binäre Datenobjekte <br> variabler Länge (z.B. Bild) | BLOB | - | - |
| Verbund | JOIN | - | - |
| JSON | JSON | '{"key": "value"}' | - |


### Inserts

#### Aufgaben 1:

a. Kurzform:

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Heinrich', 'Schmitt', 2, 'Zürich');
```

b. Kurzform:

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Sabine', 'Müller', 2, 'Bern');
```

c. Kurzform:

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Markus', 'Mustermann', 1, 'Wien');
```

d. Langform:

```sql
INSERT INTO kunden (vorname, nachname) VALUES ('Herr', 'Maier');
```

e. Langform:

```sql
INSERT INTO kunden (vorname, nachname, wohnort) VALUES ('Herr', 'Bulgur', 'Sirnach');
```

f. Langform:

```sql
INSERT INTO kunden (vorname, nachname) VALUES ('Maria', 'Manta');
```

#### Aufgaben 2:

a. Fehler: `INTO` fehlt, und die Tabelle wurde nicht spezifiziert.

```sql
INSERT INTO kunden (nachname, wohnort, land_id) VALUES ('Fesenkampp', 'Duis-burg', 3);
```

b. Fehler: Anführungszeichen um Spaltennamen.

```sql
INSERT INTO kunden (vorname) VALUES ('Herbert');
```

c. Fehler: 'Deutschland' sollte als land_id 3 eingefügt werden, nicht als 'wohnort'.

```sql
INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES ('Schulter', 'Albert', 'Duisburg', 3);
```

d. Fehler: Leerer Wert ('') ist nicht zulässig.

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('', 'Brunhild', 'Sulcher', 1, 'Süderstade');
```

e. Fehler: Falsche Anzahl von Werten.

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Jochen', 'Schmied', 2, 'Solingen');
```

f. Fehler: Leerer Wert ('') ist nicht zulässig.

```sql
INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('', 'Doppelbrecher', 2, '');
```

g. Fehler: Falsche Reihenfolge der Werte und 'INTO' fehlt.

```sql
INSERT INTO kunden (vorname, nachname, wohnort, land_id) VALUES ('Christoph', 'Fesenkampp', 'Duisburg', 3);
```

h. Korrekt.

```sql
INSERT INTO kunden (vorname) VALUES ('Herbert');
```

i. Fehler: Stringwerte müssen in Anführungszeichen stehen.

```sql
INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES ('Schulter', 'Albert', 'Duisburg', 1);
```

j. Fehler: Das Schlüsselwort für den Befehl ist `VALUES`, nicht `VALUE`. Stringwerte müssen in Anführungszeichen stehen.

```sql
INSERT INTO kunden VALUES ('', 'Brunhild', 'Sulcher', 1, 'Süderstade');
```

k. Fehler: 'Solingen' muss in Anführungszeichen stehen.

```sql
INSERT INTO kunden VALUES ('', 'Jochen', 'Schmied', 2, 'Solingen');
```

### Select

a. Alle Datensätze Ihrer DVD-Sammlung ausgeben:
```sql
SELECT * FROM dvd_sammlung;
```

b. Alle Filmtitel und die jeweils zugehörige Nummer ausgeben:
```sql
SELECT film, nummer FROM dvd_sammlung;
```

c. Alle Filmtitel und den jeweils zugehörigen Regisseur ausgeben:
```sql
SELECT film, regisseur FROM dvd_sammlung;
```

d. Alle Filme von Quentin Tarantino ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur = 'Quentin Tarantino';
```

e. Alle Filme von Steven Spielberg ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur = 'Steven Spielberg';
```

f. Alle Filme, in denen der Regisseur den Vornamen "Steven" hat, ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur LIKE 'Steven%';
```

g. Alle Filme, die länger als 2 Stunden sind, ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE laenge_minuten > 120;
```

h. Alle Filme, die von Tarantino oder von Spielberg gedreht wurden, ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur IN ('Quentin Tarantino', 'Steven Spielberg');
```

i. Alle Filme von Tarantino, die kürzer als 90 Minuten sind, ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur = 'Quentin Tarantino' AND laenge_minuten < 90;
```

j. Den Film mit "Sibirien" im Titel suchen:
```sql
SELECT * FROM dvd_sammlung WHERE film LIKE '%Sibirien%';
```

k. Alle Teile von "Das große Rennen" ausgeben:
```sql
SELECT * FROM dvd_sammlung WHERE film LIKE 'Das große Rennen%';
```

l. Alle Filme ausgeben, sortiert nach Regisseur:
```sql
SELECT * FROM dvd_sammlung ORDER BY regisseur;
```

m. Alle Filme ausgeben, sortiert nach Regisseur und dann nach Filmtitel:
```sql
SELECT * FROM dvd_sammlung ORDER BY regisseur, film;
```

n. Alle Filme von Tarantino ausgeben, die längsten zuerst:
```sql
SELECT * FROM dvd_sammlung WHERE regisseur = 'Quentin Tarantino' ORDER BY laenge_minuten DESC;
```