USE woche3;

-- CREATE TABLE kunden (kunde_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, vorname VARCHAR(255), nachname VARCHAR(255), land_id INT, wohnort VARCHAR(255));

INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Heinrich', 'Schmitt', 2, 'Zürich');

INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Sabine', 'Müller', 2, 'Bern');

INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Markus', 'Mustermann', 1, 'Wien');

INSERT INTO kunden (vorname, nachname) VALUES ('Herr', 'Maier');

INSERT INTO kunden (vorname, nachname, wohnort) VALUES ('Herr', 'Bulgur', 'Sirnach');

INSERT INTO kunden (vorname, nachname) VALUES ('Maria', 'Manta');